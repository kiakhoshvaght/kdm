package ir.haftsang.downloadmanager.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;


import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.haftsang.downloadmanager.Adapters.PagerAdapter;
import ir.haftsang.downloadmanager.BuildConfig;
import ir.haftsang.downloadmanager.DownloadRepository;
import ir.haftsang.downloadmanager.R;
import ir.haftsang.downloadmanager.RxHelper.ContentObservable;
import ir.haftsang.downloadmanager.downloadManager.AppDownloadManager;
import ir.haftsang.downloadmanager.downloadManager.DefaultRetryPolicy;
import ir.haftsang.downloadmanager.downloadManager.DownloadRequest;
import ir.haftsang.downloadmanager.fragments.BaseFragment;
import ir.haftsang.downloadmanager.services.DownloadService;
import rx.Subscriber;
import rx.Subscription;

public class BaseActivity extends AppCompatActivity implements View.OnClickListener, TabLayout.OnTabSelectedListener {

    @BindView(R.id.tab_layout_base_activity)
    protected TabLayout tabLayout;
    @BindView(R.id.view_pager_base_activity)
    protected ViewPager viewPager;

    private static final String TAG = BaseActivity.class.getName();
    private static DownloadService downloadService;
    private PagerAdapter pagerAdapter;
    private DownloadRepository downloadRepository;
    private boolean hasSearched = false;
    public final static int REQUEST_CODE = -1010101;
    private Subscription serviceStartSubscription;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "FUNCTION : onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        ButterKnife.bind(this);

        initializeUi();
        initializeTabLayout();
        startDownloadService();
        checkBuildMode();
        checkDrawOverlayPermission();
        subscribeServiceToStart();
    }

    private void initializeUi() {
        Log.i(TAG, "FUNCTION : initializeUi");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.Aqua));
        }
    }

    private void subscribeServiceToStart() {
        Log.i(TAG, "FUNCTION : subscribeServiceToStart");
        serviceStartSubscription = ContentObservable.fromBroadcast(this, new IntentFilter("service_started"))
                .subscribe(new Subscriber<Intent>() {
                    @Override
                    public void onCompleted() {
                        Log.i(TAG, "FUNCTION : subscribeServiceToStart => onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "FUNCTION : subscribeServiceToStart => onError: " + e.toString());
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Intent intent) {
                        Log.i(TAG, "FUNCTION : subscribeServiceToStart => onNext");
                        downloadService.setBaseActivity(BaseActivity.this);
                        downloadService.setIsInBackground(false);
                        downloadService.checkClipBoardText();
                    }
                });
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        Log.i(TAG, "FUNCTION : onTabSelected");
        if (tab.getPosition() == 2 && !hasSearched) {
            Log.i(TAG, "FUNCTION : onTabSelected => Search tab and has not searched");
            Toast.makeText(this, "You have not searched yet, search to show web view", Toast.LENGTH_SHORT).show();
            setSelectedTab(1);
            return;
        }
        viewPager.setCurrentItem(tab.getPosition());
    }


    private void checkBuildMode() {
        Log.i(TAG, "FUNCTION : checkBuildMode");
        if (BuildConfig.BUILD_TYPE == "debug") {
            hasSearched = true;
        }
    }


    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void startDownloadService() {
        Log.i(TAG, "FUNCTION : startDownloadService");
        Intent intent = new Intent(BaseActivity.this, DownloadService.class);
        intent.setAction(DownloadService.ACTION_START_FOREGROUND_SERVICE);
        startService(intent);
    }

    private void initializeTabLayout() {
        Log.i(TAG, "FUNCTION : initializeTabLayout");

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        tabLayout.addTab(tabLayout.newTab().setText("Download"));
        tabLayout.addTab(tabLayout.newTab().setText("Search"));
        tabLayout.addTab(tabLayout.newTab().setText("WebView"));

        tabLayout.setBackgroundColor(getResources().getColor(R.color.SlateGray));
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.Aqua));

        pagerAdapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), this);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(this);

        Objects.requireNonNull(tabLayout.getTabAt(0)).setIcon(getResources().getDrawable(R.drawable.ic_download_icon_tab_layout));
        Objects.requireNonNull(tabLayout.getTabAt(1)).setIcon(getResources().getDrawable(R.drawable.ic_search_icon_tab_layout));
        Objects.requireNonNull(tabLayout.getTabAt(2)).setIcon(getResources().getDrawable(R.drawable.ic_web_view_icon_tab_layout));
    }

    public void setSelectedTab(int index) {
        Log.i(TAG, "FUNCTION : setSelectedTab");
        TabLayout.Tab tab = tabLayout.getTabAt(index);
        tab.select();
    }

    @OnClick({
    })
    @Override
    public void onClick(View view) {
        Log.i(TAG, "FUNCTION : onClick");
        switch (view.getId()) {
        }
    }

    @Override
    public void onBackPressed() {
        Log.i(TAG, "FUNCTION : onBackPressed");
        ((BaseFragment) pagerAdapter.getItem(tabLayout.getSelectedTabPosition())).onBackPressed();
    }

    public void startFragment(final Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.view_pager_base_activity, fragment);
        transaction.addToBackStack(fragment.getTag());
        transaction.commit();
    }

    public void hideKeyboard(View view) {
        Log.i(TAG, "FUNCTION : hideKeyboard");
        if (view != null) {
            Log.i(TAG, "FUNCTION : hideKeyboard => view is not null");
            ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void startFragmentAndClearBackStack(Fragment fragment) {
        Log.i(TAG, "FUNCTION : startFragmentAndClearBackStack");
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.view_pager_base_activity, fragment);
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        transaction.addToBackStack(fragment.getTag());
        transaction.commit();
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "FUNCTION : onPause");
        DownloadRepository.getInstance(this).saveDownloadRequestArrayToSharedPreferences(this);
        downloadService.setIsInBackground(true);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "FUNCTION : onDestroy");
        unsubscribeIfNotNull(serviceStartSubscription);
        super.onDestroy();
    }

    public void checkDrawOverlayPermission() {
        Log.i(TAG, "FUNCTION : checkDrawOverlayPermission");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                Log.i(TAG, "FUNCTION : checkDrawOverlayPermission => Doesn't have");
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:ir.haftsang.downloadmanager"));
                startActivityForResult(intent, 1234);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "FUNCTION : onActivityResult");
        if (requestCode == 1234) {
            downloadService.setHasWindowPermission(true);
        }
    }

    private void unsubscribeIfNotNull(Subscription subscription) {
        Log.i(TAG, "FUNCTION : unsubscribeIfNotNull");
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }

    public AppDownloadManager getDownloadManager() {
        return downloadService.getDownloadManager();
    }

    public DownloadRequest getDownloadRequest() {
        return downloadService.getDownloadRequest();
    }

    public DefaultRetryPolicy getRetryPolicy() {
        return downloadService.getRetryPolicy();
    }

    public void setDownloadRequest(DownloadRequest downloadRequest) {
        downloadService.setDownloadRequest(downloadRequest);
    }

    public void setDownloadManager(AppDownloadManager downloadManager) {
        downloadService.setDownloadManager(downloadManager);
    }

    public void setRetryPolicy(DefaultRetryPolicy defaultRetryPolicy) {
        downloadService.setRetryPolicy(defaultRetryPolicy);
    }

    public ViewPager getViewPager() {
        return viewPager;
    }

    public TabLayout getTabLayout() {
        return tabLayout;
    }

    public static DownloadService getDownloadService() {
        return downloadService;
    }

    public static void setDownloadService(DownloadService downloadService) {
        BaseActivity.downloadService = downloadService;
    }

    public void setHasSearched(boolean hasSearched) {
        this.hasSearched = hasSearched;
    }
}


