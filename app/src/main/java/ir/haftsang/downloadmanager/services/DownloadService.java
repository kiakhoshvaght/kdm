package ir.haftsang.downloadmanager.services;

import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import ir.haftsang.downloadmanager.DownloadRepository;
import ir.haftsang.downloadmanager.R;
import ir.haftsang.downloadmanager.activity.BaseActivity;
import ir.haftsang.downloadmanager.downloadManager.AppDownloadManager;
import ir.haftsang.downloadmanager.downloadManager.DefaultRetryPolicy;
import ir.haftsang.downloadmanager.downloadManager.DownloadRequest;
import ir.haftsang.downloadmanager.downloadManager.DownloadStatusListener;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class DownloadService extends Service implements DownloadStatusListener {

    private static final String TAG = DownloadService.class.getName();

    public static final String ACTION_START_FOREGROUND_SERVICE = "ACTION_START_FOREGROUND_SERVICE";
    private static final int DOWNLOAD_THREAD_POOL_SIZE = 4;
    private static final String ACTION_CLOSE = "close";

    private AppDownloadManager downloadManager;
    private DownloadRequest downloadRequest;
    private DefaultRetryPolicy retryPolicy;
    public ArrayList<DownloadRequest> downloadRequestArray = new ArrayList<>();
    private boolean hasWindowPermission = false;
    private String lastTakenUrlFromCB;
    private boolean isInBackground = false;
    private BaseActivity baseActivity;

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "FUNCTION : onBind");
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "FUNCTION : onStartCommand");

        BaseActivity.setDownloadService(this);

        sendServiceOnlineBroadcast();
        initDownloadManager();
        makeForeground();
        checkIntent(intent);
        checkClipBoard();
        checkDrawOverlayPermission();

        return super.onStartCommand(intent, flags, startId);
    }

    private void sendServiceOnlineBroadcast() {
        sendBroadcast(new Intent("service_started"));
    }


    public void checkDrawOverlayPermission() {
        Log.i(TAG, "FUNCTION : checkDrawOverlayPermission");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.canDrawOverlays(this)) {
                hasWindowPermission = true;
            }
        }
    }

    private void checkClipBoard() {
        Log.i(TAG, "FUNCTION : checkClipBoard");
        Observable.interval(1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Long>() {
                    @Override
                    public void onCompleted() {
                        Log.i(TAG, "FUNCTION : checkClipBoard => onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "FUNCTION : checkClipBoard => onError: " + e.toString());
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Long aLong) {
                        Log.i(TAG, "FUNCTION : checkClipBoard => onNext");
                        if(isInBackground) {
                            checkClipBoardText();
                        } else {
                            Log.i(TAG,"FUNCTION : setWebViewClient => onNext => App is not in background");
                        }

                    }
                });
    }

    public void checkClipBoardText() {
        Log.i(TAG, "FUNCTION : checkClipBoardText");
        ClipboardManager clipboardManager = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
        if (clipboardManager.hasText() && URLUtil.isValidUrl(clipboardManager.getText().toString()) && hasWindowPermission && !clipboardManager.getText().equals(lastTakenUrlFromCB)) {
            Log.i(TAG, "FUNCTION : checkClipBoardText => CB has valid url");
            lastTakenUrlFromCB = clipboardManager.getText().toString();
            String lastPath = "";
            String[] paths = lastTakenUrlFromCB.split("/");
            if (paths[paths.length - 1].contains("?")) {
                lastPath = paths[paths.length - 1].substring(0, paths[paths.length - 1].indexOf("?"));
                Log.i(TAG, "FUNCTION : checkClipBoardText => last path: : " + lastPath);
            } else {
                lastPath = paths[paths.length - 1];
                Log.i(TAG, "FUNCTION : checkClipBoardText => last path: : " + lastPath);
            }
            if (checkPageType(lastPath)) {
                Log.i(TAG, "FUNCTION : checkClipBoardText => Valid url to be downloaded");
                showDownloadDialog(lastTakenUrlFromCB, lastPath);
            }
        }
    }

    private void showDownloadDialog(String url, String lastPath) {
        Log.i(TAG, "FUNCTION : showDownloadDialog");
        final Dialog dialog = new Dialog(this);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.download_dialog);
        Objects.requireNonNull(dialog.getWindow()).setType((WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY));
        dialog.findViewById(R.id.nope_btn_dialog_download).setOnClickListener(view -> dialog.dismiss());
        dialog.findViewById(R.id.ok_btn_dialog_download).setOnClickListener(view -> {
            startDownload(url, lastPath);
            Toast.makeText(DownloadService.this, "Download Started", Toast.LENGTH_SHORT).show();
            dialog.dismiss();
        });
        dialog.setCancelable(true);
        dialog.show();
    }

    private void checkIntent(Intent intent) {
        Log.i(TAG, "FUNCTION : checkIntent");
        if (intent != null && intent.getAction() != null) {
            Log.i(TAG, "FUNCTION : checkIntent => Intent is not null");
            if (intent.getAction().equals(ACTION_CLOSE)) {
                Log.i(TAG, "FUNCTION : checkIntent => Intent is not null => Intent action is close");
                stopSelf();
            }
        }
    }

    private void makeForeground() {
        Log.i(TAG, "FUNCTION : makeForeground");

        Intent notificationIntent = new Intent(this, BaseActivity.class);
        PendingIntent pendingContentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Intent closeIntent = new Intent(this, DownloadService.class);
        closeIntent.setAction(ACTION_CLOSE);
        PendingIntent closePendingIntent = PendingIntent.getService(this, 0, closeIntent, 0);

        Notification customNotification = new NotificationCompat.Builder(this, "ir.haftsang.downloadmanager")
                .setSmallIcon(R.drawable.file)
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setContentTitle("Download Manager Service")
                .setContentText("Click this to open downloads list")
                .addAction(R.drawable.red_button_background, getString(R.string.close), closePendingIntent)
                .setContentIntent(pendingContentIntent)
                .build();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel("1", "DownloadManager");
        }
        startForeground(1, customNotification);

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(String channelId, String channelName) {
        NotificationChannel chan = new NotificationChannel("ir.haftsang.downloadmanager", channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.createNotificationChannel(chan);
        return channelId;
    }

    public DefaultRetryPolicy getRetryPolicy() {
        return retryPolicy;
    }

    public void setRetryPolicy(DefaultRetryPolicy retryPolicy) {
        this.retryPolicy = retryPolicy;
    }

    private void initDownloadManager() {
        Log.i(TAG, "FUNCTION : initDownloadManager");
        if (downloadManager == null || retryPolicy == null) {
            Log.i(TAG, "FUNCTION : initDownloadManager => Objects are null and needs to be instantiated");
            downloadManager = new AppDownloadManager(DOWNLOAD_THREAD_POOL_SIZE);
            retryPolicy = new DefaultRetryPolicy();
        }

        for (DownloadRequest downloadRequest : DownloadRepository.getInstance(this).getDownloadRequestArray()) {
            downloadRequest.setStatusListener(this);
        }

        Log.i(TAG, "FUNCTION : initDownloadManager => Downloads uri: " + Environment.getExternalStorageDirectory().getPath());
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "FUNCTION : onDestroy");
        super.onDestroy();
        if (downloadManager != null) {
            downloadManager.release();
        }
    }


    @Override
    public void onDownloadComplete(DownloadRequest request) {
        Log.i(TAG, "FUNCTION : onDownloadComplete");
        request.setStatusText("Completed");
        request.setHasUpdated(true);
    }

    @Override
    public void onDownloadFailed(DownloadRequest request, int errorCode, String errorMessage) {
        Log.i(TAG, "FUNCTION : onDownloadFailed");
        request.setStatusText("Failed: ErrorCode " + errorCode + ", " + errorMessage);
        request.setProgress(0);
        request.setHasUpdated(true);
    }

    @Override
    public void onProgress(DownloadRequest request, long totalBytes, long downloadedBytes, int progress) {
//        Log.i(TAG,"FUNCTION : onProgress: " + downloadedBytes + "    " + totalBytes);
        request.setStatusText(progress + "%" + "  " + getBytesDownloaded(totalBytes, downloadedBytes));
        request.setProgress(progress);
        if(request.isPaused()){
            request.setStatusText(request.getStatusText() + "    Paused");
        }
        request.setHasUpdated(true);
    }


    private String getBytesDownloaded(long totalBytes, long downloadedBytes) {
//        Log.i(TAG,"FUNCTION : getBytesDownloaded: " + totalBytes);

        if (totalBytes >= 1000000) {
            return ("" + (String.format("%.1f", (float) downloadedBytes / 1000000)) + "/" + (String.format("%.1f", (float) totalBytes / 1000000)) + "MB");
        }
        if (totalBytes >= 1000) {
            return ("" + (String.format("%.1f", (float) downloadedBytes / 1000)) + "/" + (String.format("%.1f", (float) totalBytes / 1000)) + "Kb");

        } else {
            return ("" + downloadedBytes + "/" + totalBytes);
        }
    }


    public ArrayList<DownloadRequest> getDownloadRequestArray() {
        return downloadRequestArray;
    }

    public void setDownloadRequestArray(ArrayList<DownloadRequest> downloadRequestArray) {
        this.downloadRequestArray = downloadRequestArray;
    }

    public AppDownloadManager getDownloadManager() {
        return downloadManager;
    }

    public void setDownloadManager(AppDownloadManager downloadManager) {
        this.downloadManager = downloadManager;
    }

    public DownloadRequest getDownloadRequest() {
        return downloadRequest;
    }

    public void setDownloadRequest(DownloadRequest downloadRequest) {
        this.downloadRequest = downloadRequest;
    }

    public void setHasWindowPermission(boolean hasWindowPermission) {
        this.hasWindowPermission = hasWindowPermission;
    }


    private boolean checkPageType(String lastPath) {
        String webPagesFileExtensions =
                "asp" +
                        "|ASP.NET" +
                        "|aspx" +
                        "|axd" +
                        "|asx" +
                        "|asmx" +
                        "|ashx" +
                        "|CSS" +
                        "|css" +
                        "|Coldfusion" +
                        "|cfm" +
                        "|Erlang" +
                        "|yaws" +
                        "|Flash" +
                        "|swf" +
                        "|HTML" +
                        "|html" +
                        "|htm" +
                        "|xhtml" +
                        "|jhtml" +
                        "|Java" +
                        "|jsp" +
                        "|jspx" +
                        "|wss" +
                        "|do" +
                        "|action" +
                        "|JavaScript" +
                        "|js" +
                        "|Perl" +
                        "|pl" +
                        "|PHP" +
                        "|php" +
                        "|php4" +
                        "|php3" +
                        "|phtml" +
                        "|Python" +
                        "|py" +
                        "|Ruby" +
                        "|rb" +
                        "|rhtml" +
                        "|SSI" +
                        "|shtml" +
                        "|XML" +
                        "|xml" +
                        "|rss" +
                        "|svg" +
                        "|cgi" +
                        "|dll" +
                        "|com" +
                        "|ir" +
                        "|net" +
                        "|org" +
                        "|edu";
        String[] fileAndType = lastPath.split("\\.");
        Log.i(TAG, "FUNCTION : checkPageType => lastPath: " + lastPath);
        Log.i(TAG, "FUNCTION : checkPageType => fileAndType.length: " + fileAndType.length);
        if (fileAndType.length == 0 || fileAndType.length == 1)
            return false;
        if (!fileAndType[fileAndType.length - 1].matches(webPagesFileExtensions)) {
            return true;
        } else
            return false;
    }

    private void startDownload(String downloadURL, String fileName) {
        Log.i(TAG, "FUNCTION : startDownload");
        Uri downloadUri = Uri.parse(downloadURL);
        Uri destinationUri = Uri.parse(Environment.getExternalStorageDirectory().getPath() + "/Download/" + fileName);
        Log.i(TAG, "FUNCTION : startDownload => Download URL: " + downloadURL);
        Log.i(TAG, "FUNCTION : startDownload => File Name: " + fileName);
        DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                .setDestinationURI(destinationUri)
                .setPriority(DownloadRequest.Priority.LOW)
                .setRetryPolicy(retryPolicy)
                .setDownloadContext(fileName)
                .setStatusListener(BaseActivity.getDownloadService())
                .setName(fileName);
        DownloadRepository.getInstance(this).getDownloadRequestArray().add(downloadRequest);
        downloadManager.add(downloadRequest);
    }

    public void setBaseActivity(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    public void setIsInBackground(boolean inApp) {
        isInBackground = inApp;
    }
}
