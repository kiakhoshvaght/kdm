package ir.haftsang.downloadmanager.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.haftsang.downloadmanager.ModelClasses.SearchResult;
import ir.haftsang.downloadmanager.R;
import ir.haftsang.downloadmanager.fragments.SearchFragment;

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.ViewHolder> {

    private static final String TAG = SearchResultAdapter.class.getName();
    public ArrayList<SearchResult> searchResultArrayList = new ArrayList<>();
    private LayoutInflater mInflater;
    private SearchFragment searchFragment;

    public SearchResultAdapter(Context context, SearchFragment searchFragment) {
        Log.i(TAG, "FUNCTION : downloadsListAdapter");
        this.mInflater = LayoutInflater.from(context);
        this.searchFragment = searchFragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i(TAG, "FUNCTION : onCreateViewHolder");
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.search_result_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.i(TAG, "FUNCTION : onBindViewHolder");
        final SearchResult searchResult = searchResultArrayList.get(position);
        holder.snippetTv.setText(searchResult.getSnippet());
        holder.titleTv.setText(searchResult.getTitle());
    }

    @Override
    public int getItemCount() {
        return searchResultArrayList.size();
    }

    public ArrayList<SearchResult> getSearchResultArrayList() {
        return searchResultArrayList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.snippet_tv_search_result_item)
        public TextView snippetTv;
        @BindView(R.id.title_tv_search_result_item)
        public TextView titleTv;

        ViewHolder(View itemView) {
            super(itemView);
            Log.i(TAG, "FUNCTION : ViewHolder");
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @OnClick({
                R.id.snippet_tv_search_result_item,
                R.id.title_tv_search_result_item
        })
        @Override
        public void onClick(View view) {
            Log.i(TAG, "FUNCTION : onClick => viewId: " + view.getId());
            switch (view.getId()) {
                case R.id.snippet_tv_search_result_item:
                case R.id.title_tv_search_result_item:
                    searchFragment.startWebViewFragmentOnLinkSelected(searchResultArrayList.get(getAdapterPosition()).getLink());
                    break;
            }
        }
    }
}
