package ir.haftsang.downloadmanager.Adapters;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.haftsang.downloadmanager.DownloadRepository;
import ir.haftsang.downloadmanager.R;
import ir.haftsang.downloadmanager.downloadManager.DownloadManager;
import ir.haftsang.downloadmanager.downloadManager.DownloadRequest;
import ir.haftsang.downloadmanager.fragments.DownloadFragment;

public class DownloadsListAdapter extends RecyclerView.Adapter<DownloadsListAdapter.ViewHolder> {

    public enum DownloadStateFilter {
        ALL,
        DOWNLOADED,
        DOWNLOADING
    }

    private static final String TAG = DownloadsListAdapter.class.getName();
    private DownloadFragment downloadFragment;
    private DownloadRequest.Type typeFilter = null;
    private DownloadStateFilter downloadStateFilter = DownloadStateFilter.ALL;
    private ArrayList<DownloadRequest> downloadRequests = new ArrayList<>();

    public void setTypeFilter(DownloadRequest.Type typeFilter) {
        this.typeFilter = typeFilter;
    }

    public void setDownloadStateFilter(DownloadStateFilter downloadStateFilter) {
        this.downloadStateFilter = downloadStateFilter;
    }

    public DownloadsListAdapter(Context context, DownloadFragment downloadFragment) {
        Log.i(TAG, "FUNCTION : DownloadsListAdapter");
        this.downloadFragment = downloadFragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i(TAG, "FUNCTION : onCreateViewHolder");
        LayoutInflater layoutInflater = LayoutInflater.from(downloadFragment.getActivity());
        View listItem = layoutInflater.inflate(R.layout.download_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        Log.i(TAG, "FUNCTION : onBindViewHolder");
//        holder.setIsRecyclable(false);
        if(false) {
            holder.stateTv.setVisibility(View.VISIBLE);
            holder.stateTv.setText(downloadRequests.get(position).getDownloadState() + "");
        }
        holder.fileNameTv.setText(downloadRequests.get(position).getName());
        holder.progressTv.setText(downloadRequests.get(position).getStatusText());
        holder.downloadPb.setProgress(downloadRequests.get(position).getProgress());
        holder.downloadPb.setProgressDrawable(downloadFragment.getResources().getDrawable(R.drawable.progress_bar));
        if (downloadRequests.get(position).getDownloadState() == DownloadManager.STATUS_RUNNING && !downloadRequests.get(position).isPaused()) {
//            Log.i(TAG, "FUNCTION : onBindViewHolder => Download: " + downloadRequests.get(position).getName() + " has been played");
            holder.pausePlayBtn.setImageResource(R.drawable.pause);
        }

        if (downloadRequests.get(position).getDownloadState() == DownloadManager.STATUS_SUCCESSFUL){
            Log.i(TAG, "FUNCTION : onBindViewHolder => Download: " + downloadRequests.get(position).getName() + " has been completed");
            holder.pausePlayBtn.setImageResource(R.drawable.open);
        }
    }

    @Override
    public int getItemCount() {
//        Log.i(TAG,"FUNCTION : getItemCount => count: " + downloadRequests.size());
        downloadRequests = DownloadRepository.filter(downloadStateFilter, typeFilter);
        return downloadRequests.size();
    }


    public void notifyDataSetMightChanged() {
//        Log.i(TAG, "FUNCTION : notifyDataSetMightChanged");
        for (DownloadRequest d:downloadRequests) {
            if(d.hasUpdated()){
//                Log.i(TAG, "FUNCTION : notifyDataSetMightChanged => Download: " + d.getName() + " has updated");
                notifyItemChanged(downloadRequests.indexOf(d));
                d.setHasUpdated(false);
            }
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.download_state_tv_download_item)
        protected TextView stateTv;
        @BindView(R.id.download_progress_pb_download_item)
        public ProgressBar downloadPb;
        @BindView(R.id.progress_tv_download_item)
        public TextView progressTv;
        @BindView(R.id.filename_txt_view_download_item)
        public TextView fileNameTv;
        @BindView(R.id.pause_resume_btn_download_item)
        public ImageView pausePlayBtn;
        @BindView(R.id.cancel_btn_download_item)
        public ImageView cancelBtn;
        @BindView(R.id.cancel_ll_download_item)
        public LinearLayout cancelLl;
        @BindView(R.id.pause_resume_ll_download_item)
        public LinearLayout pauseResumeLl;

        ViewHolder(View itemView) {
            super(itemView);
//            Log.i(TAG, "FUNCTION : ViewHolder");
            ButterKnife.bind(this, itemView);
        }

        @OnClick({
                R.id.cancel_ll_download_item,
                R.id.pause_resume_ll_download_item
        })
        @Override
        public void onClick(View view) {
            Log.i(TAG, "FUNCTION : onClick => viewId: " + view.getId() + " Position: " + getAdapterPosition());
            switch (view.getId()) {
                case R.id.pause_resume_ll_download_item:
                    Log.i(TAG, "FUNCTION : onClick => Start/Pause/Resume");
                    if (downloadRequests.get(getAdapterPosition()).getDownloadDispatcher() == null || downloadRequests.get(getAdapterPosition()).isPaused()) {
                        Log.i(TAG, "FUNCTION : onClick => Start/Resume");
                        downloadRequests.get(getAdapterPosition()).setPaused(false);
                        downloadFragment.getDownloadManager().add(downloadRequests.get(getAdapterPosition()));
                        pausePlayBtn.setImageResource(R.drawable.pause);
                        progressTv.setText(R.string.connecting);
                    } else if(downloadRequests.get(getAdapterPosition()).getDownloadState() != DownloadManager.STATUS_SUCCESSFUL) {
                        Log.i(TAG, "FUNCTION : onClick => Pause");
                        downloadRequests.get(getAdapterPosition()).setPaused(true);
                        pausePlayBtn.setImageResource(R.drawable.play);
                    } else {
                        String mimeType = "";
                        try {
                            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(new URL(downloadRequests.get(getAdapterPosition()).getUri().toString()).toString()));
                            String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(mimeType);
                            Log.i(TAG, "FUNCTION : onClick => Open: " + type);
                            Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
                            intent.setDataAndType(downloadRequests.get(getAdapterPosition()).getDestinationURI(), mimeType);
                            downloadFragment.getContext().startActivity(intent);

                        } catch (Exception e) {
                            Log.i(TAG, "FUNCTION : onClick => Open => Error: " + e.toString());
                            e.printStackTrace();
                            if(e instanceof ActivityNotFoundException){
                                downloadFragment.getContext().startActivity(new Intent(android.content.Intent.ACTION_VIEW).setDataAndType(downloadRequests.get(getAdapterPosition()).getDestinationURI(),  "*/*"));
                            }
                        }

                    }
                    break;
                case R.id.cancel_ll_download_item:
                    Log.i(TAG, "FUNCTION : onClick => cancel_btn_download_item");
                    downloadRequests.get(getAdapterPosition()).cancel();
                    DownloadRepository.getInstance(downloadFragment.getContext()).getDownloadRequestArray().remove(downloadRequests.remove(getAdapterPosition()));
                    notifyDataSetChanged();
                    notifyDataSetMightChanged();
                    break;
            }
        }
    }
}