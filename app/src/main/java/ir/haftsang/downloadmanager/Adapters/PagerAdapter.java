package ir.haftsang.downloadmanager.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import ir.haftsang.downloadmanager.R;
import ir.haftsang.downloadmanager.activity.BaseActivity;
import ir.haftsang.downloadmanager.fragments.DownloadFragment;
import ir.haftsang.downloadmanager.fragments.SearchFragment;
import ir.haftsang.downloadmanager.fragments.WebViewFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {
    private static final String TAG = PagerAdapter.class.getName();
    int mNumOfTabs;
    private DownloadFragment tab1;
    private SearchFragment tab2;
    private WebViewFragment tab3;
    private FragmentManager fragmentManager;
    private BaseActivity baseActivity;


    public DownloadFragment getDownloadFragment() {
        return tab1;
    }

    public SearchFragment getSearchFragment() {
        return tab2;
    }

    public WebViewFragment getWebViewFragment() {
        return tab3;
    }

    public PagerAdapter(FragmentManager fragmentManager, int NumOfTabs, BaseActivity baseActivity) {
        super(fragmentManager);
        this.mNumOfTabs = NumOfTabs;
        this.baseActivity = baseActivity;
        this.fragmentManager = fragmentManager;
        tab3 = new WebViewFragment();
        tab1 = new DownloadFragment();
        tab2 = new SearchFragment();
    }

    @Override
    public Fragment getItem(int position) {
        Log.i(TAG, "FUNCTION : getItem");
        switch (position) {
            case 0:
                Log.i(TAG, "FUNCTION : getItem => position 0");
                return tab1;
            case 1:
                Log.i(TAG, "FUNCTION : getItem => position 1");
                return tab2;
            case 2:
                Log.i(TAG, "FUNCTION : getItem => position 2");
                return tab3;
            default:
                return null;
        }
    }


    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}