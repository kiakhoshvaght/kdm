package ir.haftsang.downloadmanager.dialog;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import ir.haftsang.downloadmanager.R;

public class SearchDialogFragment extends android.support.v4.app.DialogFragment {

    private static final String TAG = android.support.v4.app.DialogFragment.class.getName();
    public OnSelectListener onSelectedListener;
    protected RadioGroup radioGroup;
    protected RadioButton movie;
    protected  RadioButton music;
    protected  RadioButton app;
    protected ImageButton chooseBtn;
    int x,y;
    int categoryType=0;
    int screenWidth;

    public void setScreenWidth(int screenWidth) {
        this.screenWidth = screenWidth;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG,"FUNCTION : onCreateView");
        View view=inflater.inflate(R.layout.choose_category,container,false);

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Window window = getDialog().getWindow();
        // set "origin" to top left corner, so to speak
        window.setGravity(Gravity.TOP|Gravity.START);
        WindowManager.LayoutParams params = window.getAttributes();
        params.x = x + dpToPx(10);
        params.y = y + dpToPx(8);
        window.setAttributes(params);

        getDialog().getWindow().setLayout(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT);

        radioGroup= view.findViewById(R.id.radioGroup);
        movie= view.findViewById(R.id.movie);
        music= view.findViewById(R.id.music);
        app= view.findViewById(R.id.app);
        chooseBtn= view.findViewById(R.id.chooseBtn);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedId = radioGroup.getCheckedRadioButtonId();
                switch (selectedId) {
                    case R.id.movie:
                        categoryType=1;
                        break;
                    case R.id.music:
                        categoryType=2;
                        break;
                    case R.id.app:
                        categoryType=3;
                        break;
                    default:
                        break;
                }
                onSelectedListener.getSelectedItem(categoryType);
            }
        });

        chooseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        Log.i(TAG,"FUNCTION : onAttach");
        super.onAttach(context);
        try {
            onSelectedListener = (OnSelectListener) getActivity();
        }catch (ClassCastException e){
            Log.e(TAG,"FUNCTION : onAttach => Error: "+ e.getMessage());
            e.printStackTrace();
        }
    }

    public void setLayoutPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int dpToPx(float valueInDp) {
        DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }

    public interface OnSelectListener{
        void getSelectedItem(int selectedItem);
    }
}
