package ir.haftsang.downloadmanager.ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchRequest {

        @SerializedName("Keyword")
        @Expose
        private String keyword;
        @SerializedName("CategoryType")
        @Expose
        private Integer categoryType;
        @SerializedName("deviceGUID")
        @Expose
        private String deviceGUID;
        @SerializedName("AppType")
        @Expose
        private String appType;
        @SerializedName("isGuest")
        @Expose
        private Boolean isGuest;

    public SearchRequest(String keyword, Integer categoryType, String deviceGUID, String appType, Boolean isGuest) {
        this.keyword = keyword;
        this.categoryType = categoryType;
        this.deviceGUID = deviceGUID;
        this.appType = appType;
        this.isGuest = isGuest;
    }

    public String getKeyword() {
            return keyword;
        }

        public void setKeyword(String keyword) {
            this.keyword = keyword;
        }

        public Integer getCategoryType() {
            return categoryType;
        }

        public void setCategoryType(Integer categoryType) {
            this.categoryType = categoryType;
        }

        public String getDeviceGUID() {
            return deviceGUID;
        }

        public void setDeviceGUID(String deviceGUID) {
            this.deviceGUID = deviceGUID;
        }

        public String getAppType() {
            return appType;
        }

        public void setAppType(String appType) {
            this.appType = appType;
        }

        public Boolean getIsGuest() {
            return isGuest;
        }

        public void setIsGuest(Boolean isGuest) {
            this.isGuest = isGuest;
        }

    }