package ir.haftsang.downloadmanager.ModelClasses;

import com.google.gson.annotations.SerializedName;

public class BasicDownloadRequest {
    @SerializedName("uri")
    private String uri;
    @SerializedName("destination_uri")
    private String destinationUri;
    @SerializedName("download_context")
    private String downloadContext;
    @SerializedName("file_name")
    private String fileName;
    @SerializedName("state")
    private String state;
    @SerializedName("progress")
    private String progress;
    @SerializedName("status_text")
    private String statusText;

    public BasicDownloadRequest(String uri, String destinationUri, String downloadContext, String fileName, String state, String progress, String statusText) {
        this.uri = uri;
        this.destinationUri = destinationUri;
        this.downloadContext = downloadContext;
        this.fileName = fileName;
        this.state = state;
        this.progress = progress;
        this.statusText = statusText;
    }

    public String getStatusText() {
        if (statusText != null)
            return statusText;
        else
            return "";
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public String getState() {
        if (state != null)
            return state;
        else
            return "1";
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getProgress() {
        if (progress != null)
            return progress;
        else
            return "0";
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getDestinationUri() {
        return destinationUri;
    }

    public void setDestinationUri(String destinationUri) {
        this.destinationUri = destinationUri;
    }

    public String getDownloadContext() {
        return downloadContext;
    }

    public void setDownloadContext(String downloadContext) {
        this.downloadContext = downloadContext;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
