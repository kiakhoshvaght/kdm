package ir.haftsang.downloadmanager.fragments;


import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import ir.haftsang.downloadmanager.Adapters.PagerAdapter;
import ir.haftsang.downloadmanager.DownloadRepository;
import ir.haftsang.downloadmanager.R;
import ir.haftsang.downloadmanager.activity.BaseActivity;
import ir.haftsang.downloadmanager.downloadManager.DownloadRequest;

public class WebViewFragment extends BaseFragment{

    private static final String TAG = WebViewFragment.class.getName();
    private WebView mWebView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "FUNCTION : onCreateView");
        View webViewFragment = inflater.inflate(R.layout.fragment_web_view, container, false);
        mWebView = webViewFragment.findViewById(R.id.activity_main_webview);
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mWebView.loadUrl("https://soft98.ir/android/app-essential/13825-lucky-patcher-android.html");

        checkUrlBundle();
        setWebViewClient();
        return webViewFragment;
    }

    private void setWebViewClient() {
        Log.i(TAG, "FUNCTION : setWebViewClient");
        mWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView viewx, String urlx) {
                String lastPath = "";
                Log.i(TAG, "FUNCTION : setWebViewClient => NewLink: " + urlx);
                String[] paths = urlx.split("/");
                if (paths[paths.length - 1].contains("?")) {
                    lastPath = paths[paths.length - 1].substring(0, paths[paths.length - 1].indexOf("?"));
                    Log.i(TAG, "FUNCTION : setWebViewClient => last path: : " + lastPath);
                } else {
                    lastPath = paths[paths.length - 1];
                    Log.i(TAG, "FUNCTION : setWebViewClient => last path: : " + lastPath);
                }
                if (checkPageType(lastPath)) {
                    startDownload(urlx, lastPath);
                    Toast.makeText(getContext(), "Download Started", Toast.LENGTH_SHORT).show();
                    return true;
                }
                viewx.loadUrl(urlx);
                return false;
            }
        });
    }


    private void startDownload(String downloadURL, String fileName) {
        Log.i(TAG, "FUNCTION : startDownload");
        Uri downloadUri = Uri.parse(downloadURL);
        Uri destinationUri = Uri.parse(Environment.getExternalStorageDirectory().getPath() + "/Download/" + fileName);
        Log.i(TAG, "FUNCTION : startDownload => Download URL: " + downloadURL);
        Log.i(TAG, "FUNCTION : startDownload => File Name: " + fileName);
        DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                .setDestinationURI(destinationUri)
                .setPriority(DownloadRequest.Priority.LOW)
                .setRetryPolicy(((BaseActivity)getActivity()).getRetryPolicy())
                .setDownloadContext(fileName)
                .setStatusListener(BaseActivity.getDownloadService())
                .setName(fileName);
        DownloadRepository.getInstance(getContext()).getDownloadRequestArray()
                .add(downloadRequest);
        ((PagerAdapter)((BaseActivity)getActivity()).getViewPager().getAdapter()).getDownloadFragment().getDownloadManager().add(downloadRequest);
    }

    private void checkUrlBundle() {
        Log.i(TAG, "FUNCTION : checkUrlBundle");
        if (getArguments() != null && getArguments().getString("URL") != null) {
            Log.i(TAG, "FUNCTION : checkUrlBundle => We have bundle");
            mWebView.loadUrl(getArguments().getString("URL"));
            getArguments().clear();
        }
    }

    private boolean checkPageType(String lastPath) {
        String webPagesFileExtensions =
                "asp" +
                        "|ASP.NET" +
                        "|aspx" +
                        "|axd" +
                        "|asx" +
                        "|asmx" +
                        "|ashx" +
                        "|CSS" +
                        "|css" +
                        "|Coldfusion" +
                        "|cfm" +
                        "|Erlang" +
                        "|yaws" +
                        "|Flash" +
                        "|swf" +
                        "|HTML" +
                        "|html" +
                        "|htm" +
                        "|xhtml" +
                        "|jhtml" +
                        "|Java" +
                        "|jsp" +
                        "|jspx" +
                        "|wss" +
                        "|do" +
                        "|action" +
                        "|JavaScript" +
                        "|js" +
                        "|Perl" +
                        "|pl" +
                        "|PHP" +
                        "|php" +
                        "|php4" +
                        "|php3" +
                        "|phtml" +
                        "|Python" +
                        "|py" +
                        "|Ruby" +
                        "|rb" +
                        "|rhtml" +
                        "|SSI" +
                        "|shtml" +
                        "|XML" +
                        "|xml" +
                        "|rss" +
                        "|svg" +
                        "|cgi" +
                        "|dll" +
                        "|com" +
                        "|ir" +
                        "|net" +
                        "|org" +
                        "|edu";
        String[] fileAndType = lastPath.split("\\.");
        Log.i(TAG, "FUNCTION : checkPageType => lastPath: " + lastPath);
        Log.i(TAG, "FUNCTION : checkPageType => fileAndType.length: " + fileAndType.length);
        if (fileAndType.length == 0 || fileAndType.length == 1)
            return false;
        if (!fileAndType[fileAndType.length - 1].matches(webPagesFileExtensions)) {
            return true;
        } else
            return false;
    }

    public void onBackPressed() {
        Log.i(TAG, "FUNCTION : onBackPressed");
        if (mWebView.canGoBack()) {
            Log.i(TAG, "FUNCTION : onBackPressed => can go back");
            mWebView.goBack();
        } else {
            Log.i(TAG, "FUNCTION : onBackPressed => can't go back, popping back stack");
            ((BaseActivity) getActivity()).setSelectedTab(1);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        Log.i(TAG, "FUNCTION : setUserVisibleHint");
        if (isVisibleToUser) {
            Log.i(TAG, "FUNCTION : setUserVisibleHint => Is visible to user");
            if (getArguments() != null && getArguments().getString("URL") != null) {
                Log.i(TAG, "FUNCTION : setUserVisibleHint => Is visible to user => We have bundle");
                mWebView.loadUrl(getArguments().getString("URL"));
                getArguments().clear();
            }
        }
    }



}
