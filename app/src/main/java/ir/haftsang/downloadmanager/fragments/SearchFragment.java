package ir.haftsang.downloadmanager.fragments;


import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.wang.avi.AVLoadingIndicatorView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.haftsang.downloadmanager.Adapters.PagerAdapter;
import ir.haftsang.downloadmanager.Adapters.SearchResultAdapter;
import ir.haftsang.downloadmanager.Api.SearchApi;
import ir.haftsang.downloadmanager.ModelClasses.SearchRequest;
import ir.haftsang.downloadmanager.ModelClasses.SearchResponse;
import ir.haftsang.downloadmanager.R;
import ir.haftsang.downloadmanager.activity.BaseActivity;
import ir.haftsang.downloadmanager.dialog.SearchDialogFragment;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

public class SearchFragment extends BaseFragment implements View.OnClickListener, TextWatcher, RadioGroup.OnCheckedChangeListener ,TextView.OnEditorActionListener{

    private static final String TAG = SearchFragment.class.getName();
    @BindView(R.id.recycler_view_search_fragment)
    protected RecyclerView searchResultRv;
    @BindView(R.id.radio_group_search_fragment)
    protected RadioGroup radioGroup;
    @BindView(R.id.movie_radio_btn)
    protected RadioButton movie;
    @BindView(R.id.music_radio_btn)
    protected RadioButton music;
    @BindView(R.id.app_radio_btn)
    protected RadioButton app;
    @BindView(R.id.search_btn_search_fragment)
    protected ImageButton searchBtn;
    @BindView(R.id.search_et_search_fragmrnt)
    protected EditText searchEt;
    @BindView(R.id.avi_loading_search_fragment)
    protected AVLoadingIndicatorView aviLoading;

    String keyword;
    String deviceGUID = "B895260F-76DC-439C-9FF8-4FA8EDCDD96D";
    String appType = "1";
    Boolean isGuest = true;
    SearchResultAdapter searchResultAdapter;
    private int categoryType = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "FUNCTION : onCreateView");
        final View searchView = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, searchView);

        movie.setChecked(true);
        categoryType = 0;
        keyword= searchEt.getText().toString();

        searchEt.addTextChangedListener(this);
        searchEt.setOnEditorActionListener(this);

        radioGroup.setOnCheckedChangeListener(this);
        return searchView;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        int selectedId = radioGroup.getCheckedRadioButtonId();
        Log.i(TAG,"FUNCTION: onCheckedChanged : selectedId :"+ selectedId);
        switch (selectedId) {
            case R.id.movie_radio_btn:
                categoryType=1;
                break;
            case R.id.music_radio_btn:
                categoryType=2;
                break;
            case R.id.app_radio_btn:
                categoryType=3;
                break;
            default:
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    public void afterTextChanged(Editable s) {
        Log.i(TAG, "FUNCTION : onCreateView: afterTextChanged => EditText= "+ searchEt.getText().toString());
        keyword = searchEt.getText().toString();
    }

    @OnClick({
            R.id.search_btn_search_fragment
    })
    @Override
    public void onClick(View view) {
        Log.i(TAG, "FUNCTION : onClick => SearchFragment");
        switch (view.getId()) {
            case R.id.search_btn_search_fragment:
                aviLoading.setVisibility(View.VISIBLE);
                keyword = searchEt.getText().toString();
                sendKeyWord();
                break;
        }
    }

    public void sendKeyWord() {
        Log.i(TAG, "FUNCTION : sendKeyWord");
        Log.i(TAG, "FUNCTION : sendKeyWord => keyword: " + keyword);
        searchResultRv.setVisibility(View.INVISIBLE);
        SearchApi.getInstance().sendKeyWord(new SearchRequest(keyword, categoryType, deviceGUID, appType, isGuest))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<SearchResponse>() {
                    @Override
                    public void onCompleted() {
                        Log.i(TAG, "FUNCTION : sendKeyWord => onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "FUNCTION : sendKeyWord => onError: " + e.toString());
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(SearchResponse searchResponse) {
                        Log.i(TAG, "FUNCTION : sendKeyWord => onNext: " + searchResponse.getData().get(0).getTitle());
                        initializeAdapterAndRecyclerView();
                        aviLoading.setVisibility(View.INVISIBLE);
                        searchResultRv.setVisibility(View.VISIBLE);
                        DividerItemDecoration itemDecor = new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL);
                        searchResultRv.addItemDecoration(itemDecor);
                        searchResultAdapter.getSearchResultArrayList().addAll(searchResponse.getData());
                    }
                });
    }

    private void initializeAdapterAndRecyclerView() {
        Log.i(TAG, "FUNCTION : initializeAdapterAndRecyclerView");
        searchResultAdapter = new SearchResultAdapter(getActivity(), this);
        searchResultRv.setLayoutManager(new LinearLayoutManager(getContext()));
        searchResultRv.setAdapter(searchResultAdapter);
    }

    public void setCategoryType(int categoryType) {
        Log.i(TAG, "FUNCTION : setCategoryType");
        this.categoryType = categoryType;
    }

    public void setKeyWord(String keyWord) {
        Log.i(TAG, "FUNCTION : setKeyWord: " + keyWord);
        this.keyword = keyWord;
    }

    @Override
    public void onBackPressed() {
        Log.i(TAG, "FUNCTION : onBackPressed");
    }

    public void startWebViewFragmentOnLinkSelected(String Link) {
        Log.i(TAG, "FUNCTION : startWebViewFragmentOnLinkSelected");
        Bundle bundle = new Bundle();
        bundle.putString("URL", Link);
        ((BaseActivity)getActivity()).setHasSearched(true);
        ((PagerAdapter)((BaseActivity)getActivity()).getViewPager().getAdapter()).getWebViewFragment().setArguments(bundle);
        ((BaseActivity)getActivity()).setSelectedTab(2);
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        aviLoading.setVisibility(View.VISIBLE);
        keyword = searchEt.getText().toString();
        sendKeyWord();

        return false;
    }
}
