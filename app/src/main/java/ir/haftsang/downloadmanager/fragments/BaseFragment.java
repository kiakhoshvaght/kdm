package ir.haftsang.downloadmanager.fragments;

import android.support.v4.app.Fragment;

public abstract class BaseFragment extends Fragment {
    public abstract void onBackPressed();
}
