package ir.haftsang.downloadmanager.fragments;


import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.haftsang.downloadmanager.Adapters.DownloadsListAdapter;
import ir.haftsang.downloadmanager.DownloadRepository;
import ir.haftsang.downloadmanager.R;
import ir.haftsang.downloadmanager.activity.BaseActivity;
import ir.haftsang.downloadmanager.downloadManager.AppDownloadManager;
import ir.haftsang.downloadmanager.downloadManager.DownloadRequest;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;


public class DownloadFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, TextView.OnEditorActionListener {

    @BindView(R.id.recycler_view_download_fragment)
    protected RecyclerView downloadsRv;
    @BindView(R.id.download_type_spinner_fragment_download)
    protected Spinner downloadTypeSpinner;
    @BindView(R.id.downloads_state_spinner_fragment_download)
    protected Spinner downloadStateSpinner;
    @BindView(R.id.url_et_download_fragment)
    protected EditText urlEditText;


    private static final String TAG = DownloadFragment.class.getName();
    private BaseActivity baseActivity;
    private DownloadsListAdapter downloadListAdapter;
    private DownloadRepository downloadRepository;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "FUNCTION : onCreateView");
        View downloadView = inflater.inflate(R.layout.fragment_download, container, false);
        ButterKnife.bind(this, downloadView);

        initializeUi();
        setupSpinners();
        getFileAccessPermission();
        baseActivity = (BaseActivity) getActivity();
        initRecyclerView();
        updateUi();
        getDownloadBundle();

//        addSampleDownloads();

        return downloadView;
    }

    private void initializeUi() {
        Log.i(TAG, "FUNCTION : initializeUi");
        urlEditText.setOnEditorActionListener(this);
    }


    private void addSampleDownloads() {
        Log.i(TAG, "FUNCTION : addSampleDownloads");
        Uri destinationUri = Uri.parse(Environment.getExternalStorageDirectory().getPath() + "/Download/image.jpg");
        DownloadRepository.getInstance(getContext()).getDownloadRequestArray()
                .add(new DownloadRequest(Uri.parse("https://www.p30day.com/wp-content/uploads/2019/07/1-1.jpg?key=1537751"))
                        .setDestinationURI(destinationUri)
                        .setPriority(DownloadRequest.Priority.LOW)
                        .setRetryPolicy(baseActivity.getRetryPolicy())
                        .setDownloadContext("image.jpg")
                        .setStatusListener(BaseActivity.getDownloadService())
                        .setName("image.jpg"));
        destinationUri = Uri.parse(Environment.getExternalStorageDirectory().getPath() + "/Download/app.apk");
        DownloadRepository.getInstance(getContext()).getDownloadRequestArray()
                .add(new DownloadRequest(Uri.parse("http://dl2.soft98.ir/mobile/Instagram.104.0.0.0.75.v6.arm64_Soft98.iR.apk?1563620614"))
                        .setDestinationURI(destinationUri)
                        .setPriority(DownloadRequest.Priority.LOW)
                        .setRetryPolicy(baseActivity.getRetryPolicy())
                        .setDownloadContext("app.apk")
                        .setStatusListener(BaseActivity.getDownloadService())
                        .setName("app.apk"));
        destinationUri = Uri.parse(Environment.getExternalStorageDirectory().getPath() + "/Download/song.mp3");
        DownloadRepository.getInstance(getContext()).getDownloadRequestArray()
                .add(new DownloadRequest(Uri.parse("http://dls.tabanmusic.com/music/1398/04/19/Mohsen-Ebrahimzadeh-Parvanehvar.mp3"))
                        .setDestinationURI(destinationUri)
                        .setPriority(DownloadRequest.Priority.LOW)
                        .setRetryPolicy(baseActivity.getRetryPolicy())
                        .setDownloadContext("song.mp3")
                        .setStatusListener(BaseActivity.getDownloadService())
                        .setName("song.mp3"));
        destinationUri = Uri.parse(Environment.getExternalStorageDirectory().getPath() + "/Download/movie.mkv");
        DownloadRepository.getInstance(getContext()).getDownloadRequestArray()
                .add(new DownloadRequest(Uri.parse("http://dl9.dlsdm.ir/Movie/1398/04/Pokemon.Detective.Pikachu.2019.720p.x265.Pahe.mkv"))
                        .setDestinationURI(destinationUri)
                        .setPriority(DownloadRequest.Priority.LOW)
                        .setRetryPolicy(baseActivity.getRetryPolicy())
                        .setDownloadContext("movie.mkv")
                        .setStatusListener(BaseActivity.getDownloadService())
                        .setName("movie.mkv"));
    }

    private void setupSpinners() {
        Log.i(TAG, "FUNCTION : setupSpinners");
        String[] downloadStatesString = new String[]{"همه دانلود ها", "درحال دانلود", "دانلود شده"};
        String[] downloadTypesString = new String[]{"همه فایل ها", "عکس", "فیلم", "صوتی", "اپلیکیشن", "غیره"};
        ArrayAdapter<String> arrayAdapterState = new ArrayAdapter<String>(Objects.requireNonNull(getContext()), R.layout.spinner_item, downloadStatesString);
        ArrayAdapter<String> arrayAdapterType = new ArrayAdapter<String>(Objects.requireNonNull(getContext()), R.layout.spinner_item, downloadTypesString);
        downloadStateSpinner.setAdapter(arrayAdapterState);
        downloadTypeSpinner.setAdapter(arrayAdapterType);
        downloadTypeSpinner.setOnItemSelectedListener(this);
        downloadStateSpinner.setOnItemSelectedListener(this);
        downloadStateSpinner.getBackground().setColorFilter(getResources().getColor(R.color.Black), PorterDuff.Mode.SRC_ATOP);
        downloadTypeSpinner.getBackground().setColorFilter(getResources().getColor(R.color.Black), PorterDuff.Mode.SRC_ATOP);
    }


    private void getDownloadBundle() {
        Log.i(TAG, "FUNCTION : getDownloadBundle");
        Bundle bundle = this.getArguments();
        if (bundle != null && bundle.getString("DownloadURL") != null && bundle.getString("FileName") != null) {
            Log.i(TAG, "FUNCTION : getDownloadBundle => Bundle is not null");
            String downloadURL = bundle.getString("DownloadURL", "https://google.com");
            Uri downloadUri = Uri.parse(downloadURL);
            String fileName = bundle.getString("FileName", "null.file");
            Uri destinationUri = Uri.parse(Environment.getExternalStorageDirectory().getPath() + "/Download/" + fileName);
            Log.i(TAG, "FUNCTION : getDownloadBundle => Download URL: " + downloadURL);
            Log.i(TAG, "FUNCTION : getDownloadBundle => File Name: " + fileName);
            DownloadRepository.getInstance(getContext()).getDownloadRequestArray()
                    .add(new DownloadRequest(downloadUri)
                            .setDestinationURI(destinationUri)
                            .setPriority(DownloadRequest.Priority.LOW)
                            .setRetryPolicy(baseActivity.getRetryPolicy())
                            .setDownloadContext(fileName)
                            .setStatusListener(BaseActivity.getDownloadService())
                            .setName(fileName));
            downloadsRv.getAdapter().notifyDataSetChanged();
            getArguments().clear();
        }
    }

    private void initRecyclerView() {
        Log.i(TAG, "FUNCTION : initRecyclerView");
        if (downloadListAdapter == null) {
            Log.i(TAG, "FUNCTION : initRecyclerView => Adapter is null and it needs to be instantiated");
            downloadListAdapter = new DownloadsListAdapter(getActivity(), this);
        }
        DividerItemDecoration itemDecor = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        downloadsRv.addItemDecoration(itemDecor);
        downloadsRv.setLayoutManager(new LinearLayoutManager(getContext()));
        downloadsRv.setAdapter(downloadListAdapter);
        ((SimpleItemAnimator) Objects.requireNonNull(downloadsRv.getItemAnimator())).setSupportsChangeAnimations(false);
        Objects.requireNonNull(downloadsRv.getItemAnimator()).setChangeDuration(0);
    }

    @OnClick({

    })
    @Override
    public void onClick(View view) {
        Log.i(TAG, "FUNCTION : onClick");
        switch (view.getId()) {

        }
    }


    private void getFileAccessPermission() {
        Log.i(TAG, "FUNCTION : getFileAccessPermission");
        int permission = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        int REQUEST_EXTERNAL_STORAGE = 1;
        String[] PERMISSIONS_STORAGE = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    getActivity(),
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "FUNCTION : onDestroy");
        super.onDestroy();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        Log.i(TAG, "FUNCTION : setUserVisibleHint: " + isVisibleToUser);
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getDownloadBundle();
        }
    }

    public AppDownloadManager getDownloadManager() {
        return baseActivity.getDownloadManager();
    }

    @Override
    public void onBackPressed() {
        Log.i(TAG, "FUNCTION : onBackPressed");
        Objects.requireNonNull(getActivity()).finish();
    }

    private void updateUi() {
        Log.i(TAG, "FUNCTION : updateUi");
        Observable.interval(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Long>() {
                    @Override
                    public void onCompleted() {
                        Log.i(TAG, "FUNCTION : updateUi => onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "FUNCTION : updateUi => onError: " + e.toString());
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Long aLong) {
                        downloadListAdapter.notifyDataSetMightChanged();
                    }
                });
    }

    public DownloadsListAdapter getDownloadListAdapter() {
        return downloadListAdapter;
    }

    public void setDownloadListAdapter(DownloadsListAdapter downloadListAdapter) {
        this.downloadListAdapter = downloadListAdapter;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Log.i(TAG, "FUNCTION : onItemSelected => item: " + i);
        if (view != null) {
            Log.i(TAG, "FUNCTION : onItemSelected => View is not null");
            switch (adapterView.getId()) {
                case R.id.downloads_state_spinner_fragment_download:
                    Log.i(TAG, "FUNCTION : onItemSelected => State spinner");
                    switch (i) {
                        case 0:
                            Log.i(TAG, "FUNCTION : onItemSelected => State spinner => 0: ALL");
                            downloadListAdapter.setDownloadStateFilter(DownloadsListAdapter.DownloadStateFilter.ALL);
                            break;
                        case 1:
                            Log.i(TAG, "FUNCTION : onItemSelected => State spinner => 1: DOWNLOADING");
                            downloadListAdapter.setDownloadStateFilter(DownloadsListAdapter.DownloadStateFilter.DOWNLOADING);
                            break;
                        case 2:
                            Log.i(TAG, "FUNCTION : onItemSelected => State spinner => 2: DOWNLOADED");
                            downloadListAdapter.setDownloadStateFilter(DownloadsListAdapter.DownloadStateFilter.DOWNLOADED);
                            break;
                    }
                    break;
                case R.id.download_type_spinner_fragment_download:
                    Log.i(TAG, "FUNCTION : onItemSelected => Type spinner");
                    switch (i) {
                        case 0:
                            Log.i(TAG, "FUNCTION : onItemSelected => Type spinner => 0: ALL");
                            downloadListAdapter.setTypeFilter(null);
                            break;
                        case 1:
                            Log.i(TAG, "FUNCTION : onItemSelected => Type spinner => 1: IMAGE");
                            downloadListAdapter.setTypeFilter(DownloadRequest.Type.IMAGE);
                            break;
                        case 2:
                            Log.i(TAG, "FUNCTION : onItemSelected => Type spinner => 2: VIDEO");
                            downloadListAdapter.setTypeFilter(DownloadRequest.Type.VIDEO);
                            break;
                        case 3:
                            Log.i(TAG, "FUNCTION : onItemSelected => Type spinner => 3: AUDIO");
                            downloadListAdapter.setTypeFilter(DownloadRequest.Type.AUDIO);
                            break;
                        case 4:
                            Log.i(TAG, "FUNCTION : onItemSelected => Type spinner => 4: APP");
                            downloadListAdapter.setTypeFilter(DownloadRequest.Type.APPLICATION);
                            break;
                        case 5:
                            Log.i(TAG, "FUNCTION : onItemSelected => Type spinner => 5: OTHER");
                            downloadListAdapter.setTypeFilter(DownloadRequest.Type.OTHER);
                            break;
                    }
                    break;
            }
            downloadListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    private String getFileName(String url){
        Log.i(TAG, "FUNCTION : getFileName");
        String[] fileAndType = url.split("\\.");
        return fileAndType[fileAndType.length - 2];
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        Log.i(TAG, "FUNCTION : onEditorAction");
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            String downloadURL = urlEditText.getText().toString();
            String fileName = getFileName(downloadURL);
            Uri downloadUri = Uri.parse(downloadURL);
            Uri destinationUri = Uri.parse(Environment.getExternalStorageDirectory().getPath() + "/Download/" + fileName);
            Log.i(TAG, "FUNCTION : onEditorAction => Download URL: " + downloadURL);
            Log.i(TAG, "FUNCTION : onEditorAction => File Name: " + fileName);
            DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                    .setDestinationURI(destinationUri)
                    .setPriority(DownloadRequest.Priority.LOW)
                    .setRetryPolicy(baseActivity.getRetryPolicy())
                    .setDownloadContext(fileName)
                    .setStatusListener(BaseActivity.getDownloadService())
                    .setName(fileName);
            DownloadRepository.getInstance(getContext()).getDownloadRequestArray()
                    .add(downloadRequest);
            getDownloadManager().add(downloadRequest);
            downloadsRv.getAdapter().notifyDataSetChanged();
            showDownloadStartedToast();
        }
        return false;
    }

    public void showDownloadStartedToast(){
        Log.i(TAG, "FUNCTION : showDownloadStartedToast");
        Toast.makeText(this.getContext(), "Download Started", Toast.LENGTH_SHORT).show();
    }

}
