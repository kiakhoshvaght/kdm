package ir.haftsang.downloadmanager.enums;

public enum AppType {
    ANDROID("Android", 1),
    WEB("Web", 2);

    private String stringValue;
    private int intValue;

    private AppType(String toString, int value) {
        stringValue = toString;
        intValue = value;
    }
}
