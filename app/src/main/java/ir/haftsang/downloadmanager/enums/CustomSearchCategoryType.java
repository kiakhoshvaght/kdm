package ir.haftsang.downloadmanager.enums;

public enum CustomSearchCategoryType {
    Movie("ویدیو", 1),
    Music("موزیک", 2),
    App("اپلیکبشن",3);

    private String stringValue;
    private int intValue;

    CustomSearchCategoryType(String toString, int value) {
        stringValue = toString;
        intValue = value;
    }
}