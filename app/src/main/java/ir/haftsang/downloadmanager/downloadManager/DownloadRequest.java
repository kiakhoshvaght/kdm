package ir.haftsang.downloadmanager.downloadManager;

import android.net.Uri;
import android.util.Log;

import java.util.HashMap;

public class DownloadRequest implements Comparable<DownloadRequest> {

    private static final String TAG = DownloadRequest.class.getName();

    public enum Priority {
        LOW,
        NORMAL,
        HIGH,
        IMMEDIATE
    }

    public enum Type {
        IMAGE,
        AUDIO,
        APPLICATION,
        VIDEO,
        COMPRESSED,
        OTHER
    }

    private int mDownloadState;
    private int mDownloadId;
    private Uri mUri;
    private Uri mDestinationURI;
    private RetryPolicy mRetryPolicy;
    private boolean mCancelled = false;
    private boolean mDeleteDestinationFileOnFailure = true;
    private DownloadRequestQueue mRequestQueue;
    private DownloadProgressListener mDownloadListener;
    private DownloadStatusListener mDownloadStatusListener;
    private Object mDownloadContext;
    private HashMap<String, String> mCustomHeader;
    private Priority mPriority = Priority.NORMAL;
    private boolean isDownloadResumable = false;
    private boolean isPaused = false;
    private DownloadDispatcher downloadDispatcher;
    private String name;
    private String statusText;
    private int progress;
    private Type type;
    private boolean hasUpdated = false;

    public DownloadRequest setmDownloadState(int mDownloadState) {
        this.mDownloadState = mDownloadState;
        return this;
    }

    public boolean hasUpdated() {
        return hasUpdated;
    }

    public void setHasUpdated(boolean hasUpdated) {
        this.hasUpdated = hasUpdated;
    }

    public Type getType() {
        return type;
    }

    public void setType() {
        String imageFileExtensions =
                "bmp" +
                        "|dds" +
                        "|gif" +
                        "|heic" +
                        "|jpg" +
                        "|png" +
                        "|psd" +
                        "|pspimage" +
                        "|tga" +
                        "|thm" +
                        "|tif" +
                        "|tiff" +
                        "|yuv";

        String videoFileExtensions =
                "3g2" +
                        "|3gp" +
                        "|asf" +
                        "|avi" +
                        "|flv" +
                        "|m4v" +
                        "|mov" +
                        "|mp4" +
                        "|mpg" +
                        "|rm" +
                        "|srt" +
                        "|swf" +
                        "|vob" +
                        "|vob" +
                        "|mkv" +
                        "|wma";

        String audioFileExtensions =
                "aif" +
                        "|iff" +
                        "|m3u" +
                        "|m4a" +
                        "|mid" +
                        "|mp3" +
                        "|mpa" +
                        "|wav" +
                        "|wma" +
                        "|flac";


        String apkFileExtension = "apk";


        String compressedFileExtensions =
                "zip" +
                        "|rar";

        String[] fileAndType = name.split("\\.");

        Log.i(TAG, "FUNCTION : setType => file Type: " + fileAndType[fileAndType.length - 1]);

        if (fileAndType[fileAndType.length - 1].matches(imageFileExtensions)) {
            type = Type.IMAGE;
        } else if (fileAndType[fileAndType.length - 1].matches(videoFileExtensions)) {
            type = Type.VIDEO;
        } else if (fileAndType[fileAndType.length - 1].matches(audioFileExtensions)) {
            type = Type.AUDIO;
        } else if (fileAndType[fileAndType.length - 1].matches(apkFileExtension)) {
            type = Type.APPLICATION;
        } else if (!fileAndType[fileAndType.length - 1].matches(compressedFileExtensions)) {
            type = Type.COMPRESSED;
        } else {
            type = Type.OTHER;
        }
    }

    public String getStatusText() {
        return statusText;
    }

    public DownloadRequest setStatusText(String statusText) {
        this.statusText = statusText;
        return this;
    }

    public int getProgress() {
        return progress;
    }

    public DownloadRequest setProgress(int progress) {
        this.progress = progress;
        return this;
    }

    public boolean isPaused() {
        return isPaused;
    }

    public void setPaused(boolean paused) {
        isPaused = paused;
        downloadDispatcher = null;
        if (paused) {
            mDownloadState = DownloadManager.DOWNLOAD_PAUSED;
        }
    }

    public DownloadRequest(Uri uri) {
        if (uri == null) {
            throw new NullPointerException();
        }

        String scheme = uri.getScheme();
        if (scheme == null || (!scheme.equals("http") && !scheme.equals("https"))) {
            throw new IllegalArgumentException("Can only download HTTP/HTTPS URIs: " + uri);
        }

        mCustomHeader = new HashMap<>();
        mDownloadState = DownloadManager.STATUS_PENDING;
        mUri = uri;

        if(name!=null && !name.equals("")){
            setType();
        }
    }

    public Priority getPriority() {
        return mPriority;
    }

    public DownloadRequest setPriority(Priority priority) {
        mPriority = priority;
        return this;
    }

    public DownloadRequest addCustomHeader(String key, String value) {
        mCustomHeader.put(key, value);
        return this;
    }

    void setDownloadRequestQueue(DownloadRequestQueue downloadQueue) {
        mRequestQueue = downloadQueue;
    }

    public RetryPolicy getRetryPolicy() {
        return mRetryPolicy == null ? new DefaultRetryPolicy() : mRetryPolicy;
    }

    public DownloadRequest setRetryPolicy(RetryPolicy mRetryPolicy) {
        this.mRetryPolicy = mRetryPolicy;
        return this;
    }

    public final int getDownloadId() {
        return mDownloadId;
    }

    final void setDownloadId(int downloadId) {
        mDownloadId = downloadId;
    }

    public int getDownloadState() {
        return mDownloadState;
    }

    void setDownloadState(int mDownloadState) {
        hasUpdated = true;
        this.mDownloadState = mDownloadState;
    }

    DownloadProgressListener getDownloadListener() {
        return mDownloadListener;
    }

    @Deprecated
    public DownloadRequest setDownloadListener(DownloadProgressListener downloadListener) {
        this.mDownloadListener = downloadListener;
        return this;
    }

    DownloadStatusListener getStatusListener() {
        return mDownloadStatusListener;
    }

    public DownloadRequest setStatusListener(DownloadStatusListener downloadStatusListenerV1) {
        mDownloadStatusListener = downloadStatusListenerV1;
        return this;
    }

    public Object getDownloadContext() {
        return mDownloadContext;
    }

    public DownloadRequest setDownloadContext(Object downloadContext) {
        mDownloadContext = downloadContext;
        return this;
    }

    public Uri getUri() {
        return mUri;
    }

    public DownloadRequest setUri(Uri mUri) {
        this.mUri = mUri;
        return this;
    }

    public Uri getDestinationURI() {
        return mDestinationURI;
    }

    public DownloadRequest setDestinationURI(Uri destinationURI) {
        this.mDestinationURI = destinationURI;
        return this;
    }

    public boolean getDeleteDestinationFileOnFailure() {
        return mDeleteDestinationFileOnFailure;
    }

    public DownloadRequest setDownloadResumable(boolean isDownloadResumable) {
        this.isDownloadResumable = isDownloadResumable;
        setDeleteDestinationFileOnFailure(false);
        return this;
    }

    public boolean isResumable() {
        return isDownloadResumable;
    }

    public DownloadRequest setDeleteDestinationFileOnFailure(boolean deleteOnFailure) {
        this.mDeleteDestinationFileOnFailure = deleteOnFailure;
        return this;
    }

    public void cancel() {
        mCancelled = true;
    }

    public boolean isCancelled() {
        return mCancelled;
    }

    public void abortCancel() {
        mCancelled = false;
    }

    HashMap<String, String> getCustomHeaders() {
        return mCustomHeader;
    }

    void finish() {
        mRequestQueue.finish(this);
    }

    @Override
    public int compareTo(DownloadRequest other) {
        Priority left = this.getPriority();
        Priority right = other.getPriority();

        // High-priority requests are "lesser" so they are sorted to the front.
        // Equal priorities are sorted by sequence number to provide FIFO ordering.
        return left == right ?
                this.mDownloadId - other.mDownloadId :
                right.ordinal() - left.ordinal();
    }

    public DownloadDispatcher getDownloadDispatcher() {
        return downloadDispatcher;
    }

    public void setDownloadDispatcher(DownloadDispatcher downloadDispatcher) {
        this.downloadDispatcher = downloadDispatcher;
    }

    public String getName() {
        return name;
    }

    public DownloadRequest setName(String name) {
        this.name = name;
        if(name!=null && !name.equals("")){
            setType();
        }
        return this;
    }
}