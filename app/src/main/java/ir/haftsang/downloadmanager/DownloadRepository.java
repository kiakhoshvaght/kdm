package ir.haftsang.downloadmanager;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import ir.haftsang.downloadmanager.Adapters.DownloadsListAdapter.DownloadStateFilter;
import ir.haftsang.downloadmanager.Helper.SharedPreferencesHelper;
import ir.haftsang.downloadmanager.ModelClasses.BasicDownloadRequest;
import ir.haftsang.downloadmanager.downloadManager.DefaultRetryPolicy;
import ir.haftsang.downloadmanager.downloadManager.DownloadManager;
import ir.haftsang.downloadmanager.downloadManager.DownloadRequest;
import ir.haftsang.downloadmanager.downloadManager.RetryPolicy;

public class DownloadRepository {

    private static final String TAG = DownloadRepository.class.getName();
    private static ArrayList<DownloadRequest> downloadRequestArray = new ArrayList<>();
    private static DownloadRepository downloadRepository;
    private RetryPolicy retryPolicy = new DefaultRetryPolicy();

    private DownloadRepository(Context context) {
        Log.i(TAG, "FUNCTION : DownloadRepository");
        downloadRequestArray = new ArrayList<>();
        getDownloadListFromSharedPreferences(context);
    }

    public static DownloadRepository getInstance(Context context) {
//        Log.i(TAG, "FUNCTION : getDownloadRequestArray");
        if (downloadRepository == null) {
            Log.i(TAG, "FUNCTION : getInstance => Instance is null, going to instantiate it");
            downloadRepository = new DownloadRepository(context);
        }
        return downloadRepository;
    }

    public ArrayList<DownloadRequest> getDownloadRequestArray() {
//        Log.i(TAG, "FUNCTION : getDownloadRequestArray");
        return downloadRequestArray;
    }

    public void setDownloadRequestArray(ArrayList<DownloadRequest> downloadRequestArray) {
//        Log.i(TAG, "FUNCTION : setDownloadRequestArray");
        DownloadRepository.downloadRequestArray = downloadRequestArray;
    }

    public void saveDownloadRequestArrayToSharedPreferences(Context context) {
        Log.i(TAG, "FUNCTION : setDownloadRequestArrayToSharedPreferences");
        ArrayList<BasicDownloadRequest> basicDownloadRequests = new ArrayList<>();
        for (DownloadRequest downloadRequestItem : downloadRequestArray) {
            try {
                basicDownloadRequests.add(new BasicDownloadRequest(
                        new URL(downloadRequestItem.getUri().toString()).toString(),
                        downloadRequestItem.getDestinationURI().getPath(),
                        (String) downloadRequestItem.getDownloadContext(),
                        downloadRequestItem.getName(), downloadRequestItem.getDownloadState() + "", downloadRequestItem.getProgress() + "", downloadRequestItem.getStatusText()));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        String downloadsJson = new Gson().toJson(basicDownloadRequests);
        Log.i(TAG, "FUNCTION : setDownloadRequestArrayToSharedPreferences => Json: " + downloadsJson);
        SharedPreferencesHelper.put(context, SharedPreferencesHelper.Property.DOWNLOAD_LIST, downloadsJson);
    }

    private void getDownloadListFromSharedPreferences(Context context) {
        Log.i(TAG, "FUNCTION : getDownloadListFromSharedPreferences");
        String downloadRequestsJson = SharedPreferencesHelper.get(context, SharedPreferencesHelper.Property.DOWNLOAD_LIST, "");
        Log.i(TAG, "FUNCTION : getDownloadListFromSharedPreferences => Download request json: " + downloadRequestsJson);
        if (!downloadRequestsJson.equals("")) {
            Type type = new TypeToken<List<BasicDownloadRequest>>() {
            }.getType();
            ArrayList<BasicDownloadRequest> temporaryDownloadRequestArray = new Gson().fromJson(downloadRequestsJson, type);
            Log.i(TAG, "FUNCTION : getDownloadListFromSharedPreferences => temporaryDownloadRequestArray is NOT null");
            for (BasicDownloadRequest basicDownloadRequest : temporaryDownloadRequestArray) {
                Log.i(TAG, "FUNCTION : getDownloadListFromSharedPreferences => temporaryDownloadRequestArray is NOT null => This item name: " + basicDownloadRequest.getFileName());
                downloadRequestArray.add(new DownloadRequest(Uri.parse(basicDownloadRequest.getUri()))
                        .setDestinationURI(Uri.parse(basicDownloadRequest.getDestinationUri()))
                        .setName(basicDownloadRequest.getFileName())
                        .setDownloadContext(basicDownloadRequest.getDownloadContext())
                        .setPriority(DownloadRequest.Priority.LOW)
                        .setRetryPolicy(retryPolicy)
                        .setProgress(Integer.parseInt(basicDownloadRequest.getProgress()))
                        .setmDownloadState(Integer.parseInt(basicDownloadRequest.getState()))
                        .setStatusText(basicDownloadRequest.getStatusText()));
            }
        }
    }

    public static ArrayList<DownloadRequest> filter(DownloadStateFilter downloadStateFilter, DownloadRequest.Type type) {
//        Log.i(TAG, "FUNCTION : filter");
        ArrayList<DownloadRequest> downloadRequests = new ArrayList<>();
        for (DownloadRequest downloadRequest : downloadRequestArray) {
//            Log.i(TAG, "FUNCTION : filter => In selecting loop");
            if (type != null) {
//                Log.i(TAG, "FUNCTION : filter => type filter is not null");
                if (downloadRequest.getType() != type) {
//                    Log.i(TAG, "FUNCTION : filter => type filter is not null => This item is not from selected type");
                    continue;
                }
            }
            if (downloadStateFilter != DownloadStateFilter.ALL) {
//                Log.i(TAG, "FUNCTION : filter => type filter is not ALL");
                if (downloadStateFilter == DownloadStateFilter.DOWNLOADED && downloadRequest.getDownloadState() != DownloadManager.STATUS_SUCCESSFUL) {
//                    Log.i(TAG, "FUNCTION : filter => type filter is not ALL => This item is not completed");
                    continue;
                }
                if (downloadStateFilter == DownloadStateFilter.DOWNLOADING && downloadRequest.getDownloadState() == DownloadManager.STATUS_SUCCESSFUL) {
//                    Log.i(TAG, "FUNCTION : filter => type filter is not ALL => This item is completed");
                    continue;
                }
            }
            downloadRequests.add(downloadRequest);
        }
        return downloadRequests;
    }

}
