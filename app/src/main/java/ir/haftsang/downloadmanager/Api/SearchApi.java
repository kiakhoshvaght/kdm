package ir.haftsang.downloadmanager.Api;

        import android.util.Log;
        import com.facebook.stetho.okhttp3.StethoInterceptor;
        import java.util.concurrent.TimeUnit;

        import ir.haftsang.downloadmanager.BuildConfig;
        import ir.haftsang.downloadmanager.ModelClasses.SearchRequest;
        import ir.haftsang.downloadmanager.ModelClasses.SearchResponse;
        import okhttp3.OkHttpClient;
        import retrofit2.Retrofit;
        import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
        import retrofit2.converter.gson.GsonConverterFactory;
        import retrofit2.http.Body;
        import retrofit2.http.Headers;
        import retrofit2.http.POST;
        import rx.Observable;
        import rx.schedulers.Schedulers;

public class SearchApi{

    private static final String TAG = SearchApi.class.getName();
    private SearchWeb search;
    private static SearchApi apiService;

    public static SearchApi getInstance() {
        Log.i(TAG, "FUNCTION : getInstance");
        if (apiService == null) {
            Log.i(TAG, "FUNCTION : getInstance => Instance is null, going to instantiate");
            apiService = new SearchApi();
            apiService.init();
            return apiService;
        } else {
            Log.i(TAG, "FUNCTION : getInstance => Instance is not null, going to return instance");
            return apiService;
        }
    }

    public void init() {
        Log.i(TAG, "FUNCTION : init");
        OkHttpClient httpClient = new OkHttpClient();
        httpClient = new OkHttpClient.Builder().addNetworkInterceptor(new StethoInterceptor()).build();
        try {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build();
            search = retrofit.create(SearchWeb.class);
        } catch (Exception e) {
            Log.e(TAG, "FUNCTION : init => Error: " + e.toString());
            e.printStackTrace();
        }
        Log.i(TAG, "FUNCTION : init => Here");
    }

    public Observable<SearchResponse> sendKeyWord(SearchRequest searchRequest) {
        Log.i(TAG, "FUNCTION : sendActivationCode");
        return search.getLinks(searchRequest)
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .retry(2)
                .timeout(10, TimeUnit.SECONDS);
    }

    interface SearchWeb {
        @Headers("Content-Type: application/json")
        @POST("service/customsearchapi")
        Observable<SearchResponse> getLinks(@Body SearchRequest searchRequest);
    }

}
